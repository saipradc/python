print('hello world')

a=1
if a > 0:
    print(a)
else:

    print("nothing") #commment

#comments

'''
this is a multiline comment
'''

"""
this is a multiline comment
"""

#variables

x=5
y=10

x1='5'
print(type(x1))

try:
    x2 = x1 * x1
except:
    print("type Error")
else:
    x1 = int(x1)
    x2 = x1 * x1
    print(x2)

y2="john"

#type casting

y_new = int(x1)

print(type(y_new))

print(y_new * y_new)

_name = "some"
name = "some"
#2name = ""
name1 = ""
na1me = ""

name = "john"
NAME = "john"

name1 = name2 = name3 = "akhil"

print(name1)
print(name2) 
print(name3)

# arrays
axis = ["x", "y", "z"]
#x, y, z = axis

for i in axis:
    print(i)
# scope of a variable.

# method
def myfun():
    global x
    x = 6
    print(x) #local 

myfun()

print(x) #global
    
# data types


print(type(x))

"""
text type: str
numeric type: int, float, complex
seq: list, tuple, range
mapping type: dict
set: set, frozenset
boolean type: bool
binary types: bytes, bytearray, memomoryview
NOne type: NoneType
"""

# Strings and string manip

a = """ hello
text type: str
numeric type: int, float, complex
seq: list, tuple, range
mapping type: dict
set: set, frozenset
boolean type: bool
binary types: bytes, bytearray, memomoryview
NOne type: NoneType
""" 
b = "this is python"

a = "class" # remove the white spaces
#lstrip and rstrip

print(a.lstrip("th"))

print(a.rstrip("on"))

#print(a.replace("t", "T"))

# quite similler but 
print(a.split("/"))

print(a.rsplit("/", 0))


c = a +": "+ b
print(c)

d = 'this is python {} {} {}'
print(d.format("class", "devops", "data engineers"))

# operators
"""
arithmatic operators
*+-/%**//
assignment operators
=, +=, -+,*=, /=, ^=
x=5, x+=5 x = x+5
comparission operators
==, !=, >, < <=, >=
logical operators and, or, not
identity operators: is, is not

membership operators : in and not in
bitwise operators:  & | ^ ~ << , >>
"""


x = 5
print(x)
x += 5 # for loops, or while loops 
print(x)
c="s"

b = "this is b"

a = "this is a"

if a is not b:
    print(a)
else:
    print(b)

# Lists

a = ["10", "2", "13"]
b = list(("a", "b", "c"))
#orderd, changeble, it will allow duplicates
# list items are indexed, [0] [...]

# caluculating the length

print(len(a))

print(b[0:2])

pwd = "/Users/saipradeepchebrolu/python"

array = pwd.split("/")

print(array[:-1])

# changing the data inside the list

array[1:3] = ["usr", "sai"]


array.insert(0, "root")

array.append("new.py")

print(array)

#removing the elements from the list
#del array[4]
#array.pop(0)
array.remove("")

print(array)

#array.clear()

#print(array)
# while accesing the list elements, I want to loop over the elements from the list

#For, while
for x in array:
    if x == "sai":
        print(f'{x} is in the list')
    else:
       print(f'{x} is not in the list')
# loop over the list with indexes

length = len(array)
l2 = []
for x in array:
    l2.insert(0, x)
print(l2)
    #del array[x]
    #array[x] = array[length]
    #length -= 1
print(array) 

#    if array[x] == "sai":
#        print(array[x])
#    else:
#        print(f'not found in the list {x}')

#[print(f'thi is printing from the list comprehension {x}') for x in array]
#using while loops on an array
#i = 1

#while i <= 10:
#    print(i)
#    i += 1
numeric_list = [10, 2, 4, 5, 9, 1, 10, 20, 30, 25]
#print(numeric_list.sort())
sorted_array = numeric_list.reverse()
#print(sorted_array)

new_list = array + numeric_list

print(new_list)
#array.extend(numeric_list)

print(array)

for x in numeric_list:
    if x == 10 :
       array.append(11) 
    else:
        array.append(x)

print(array)

# tuple, set, dict
t = ("a", "b", "c", "a", "b")
print(type(t))
#ordered and unchangable.
# it will allow duplicates.
# access tuple using index starting 0,1...

# once tuple is created you can not add or remove items.

print(t[2:5])

# you will create an another tuple inorder to add the elements

t1 = ("c","d")

t += t1
print(t)

#loops in the tuple

for i in t:
    print(i)

for i in range(len(t)):
    print(t[i])

i = 7
while i > len(t):
    print(t[i])
    i -=1 

# set and dict 
# {}
# unordered, unchangable, unidexed
# it will not allow any duplicates.
# True and 1 will be considered as same.
# same tuple, once the set is create you can not change its itesm.
# you can add items to the set. add()      
s = {"ab", "bc", "cd", "ae", "be", "cd", True, 1, False, 0}
a = {"ab", 2, True}
b = ("a", 1, True)
#d = {"a": 1, "b": 2, "c": 3, 1: 3}

s.add("e")
s.update(a)

for i in s:
    print(i)


print(s)

s.union(a, b)
s.difference(a, b)
s.symmetric_difference(a)
s.intersection(a, b)


print(s.union(t, array))
# Union and update methods works the same, intersection, difference, symmetric_difference

# dictionaries
#d = {"a": 1, "b": 2, "c": 3, 1: 3}
d = {"a": ["1", "2", 3, 4], "b": {"x", "y", "z"}, "c": ("2", 3, 4), "d": { "a": 1, "b": 2}}

print(d["d"]["a"])


for x in range(len(d["a"])):
    if type(d["a"][x]) is str:
        d["a"][x] = int(d["a"][x])
        print(type(d["a"][x]))

print(d["a"])



for x in d["c"]:
    if type(x) is str:
        y = int(x)
        print(x, type(y))
    else:
        print(x)

print(d["c"])

json_obj = {"classdetails": [{"user":1, "user_name": "afreen", "feild": "DevOpS"}, {"user":2, "user_name": "manoj", "feild": "Data engineering"}, {"user":3, "user_name": "mahesh babu", "feild": "DevOps"}]}

for i in json_obj["classdetails"]:
    if i["user"] == 2:
        print(i["user_name"])

    #print(f'user id: {i["user"]} user name: {i["user_name"]}')


new_dict = {"name": "akhil", "experience": "8 years", "location": "st.louis"}
#print(new_dict)
city  = "texas"
new_dict.update({"location": city })
print(new_dict)

new_dict["feild"] = "DevOps"

new_dict.update({"fav_place": city })
new_dict.pop("fav_place")
new_dict.popitem()
#del new_dict
print(new_dict)

#printing keys
for x in new_dict:
    print(x)

for x in new_dict.keys():
    print(x)

#printing values
for x in new_dict:
    print(new_dict[x])

for x in new_dict.values():
    print(x)


# if you wanted to loop through both keys and values

for x, y in new_dict.items():
    print(x +": "+ y)

print(d)

dict_in_dict = d["d"]

print(dict_in_dict)

json_obj1 = {"classdetails": [{"user":1, "user_name": "afreen", "feild": "DevOpS"}, {"user":2, "user_name": "manoj", "feild": "Data engineering"}, {"user":3, "user_name": "mahesh babu", "feild": "DevOps"}],
             "Guesthouse": [{"user":1, "user_name": "chandana", "feild": "DevOpS"}, {"user":2, "user_name": "manoj", "feild": "Data engineering"}, {"user":3, "user_name": "mahesh babu", "feild": "DevOps"}],
             "remote": [{"user":1, "user_name": "afreen", "feild": "DevOpS"}, {"user":2, "user_name": "srujana", "feild": "Data engineering"}]
             }


#print(json_obj1)

for x in json_obj1:
    if x == "Guesthouse":
        guesthouse = json_obj1[x]
        for y in guesthouse:
            print(y["user_name"]) if y["user"] == 2 else print(y["feild"]) if y["user_name"] == "manoj" else y["user"] == 1 
            print(y)

# Functions
# it is a bloack of code can be reusable, and runs when ever we calls this function. 
# we can pass objectes, parameters, data
# return data as a result of the function execution
        
# def to define a function

def myfunc(category):
    for x in category:
        type = json_obj1[x]
        for y in type:
            print("students in", x, y["user_name"])

keys = ["Guesthouse", "remote", "classdetails"]


myfunc(keys)


def fun_with_param(*name):
# names is the tuple datatype
    for n in name:
        print("this is:", n)

fun_with_param("manoj", "rahul", "chandana", "mahesh", "vamsi")

#fun_with_param("devopss")

#passing the params Kargs
def kargs(**name):
    print(name)

kargs(manoj = "guesthouse student", akhil = "guest house student")

def new_fun(name = "akhil"):
    print(name)

new_fun()

# lambda is a small anonymous function
# it can take any nmumber of arguments, but can only have one expression

#lambda args : expression

a = 5
b = 10
c = a + b
print(c)

x = lambda a, b, c, d : a + b + c + d * 10
print(x(5, 10, 13, 14))

def add(n):
    return lambda a : a + n

def multi(n):
    return lambda a : a * n

a = add(1)
m = multi(4)

print(a(10), m(10))

import numpy

# object oriented principles of python.
# creating a class
class Firstclass:
    x = 5
    y = 6
    def __init__(self):
        pass
    def add(self, n):
        addition = self.x + n
        return addition
    def multi(self, n):
        multiplication = self.y * n 
        print(multiplication)
        return multiplication

# creating an object

p1  = Firstclass()
n = 6
p1.add(n)

class members:
    def __init__(initmethod, name, age):
        initmethod.name = name
        initmethod.age = age
    def newmethod(newfunc):
        print(newfunc.name, newfunc.age)

m1 = members("akhil","25")

m2 = members("manoj","25")

m1.newmethod()
m2.newmethod()

# inheritence
#parent class and a child class

class student(members):
    def __init__(self, name, age): # this no longer have access to the parent class variables
       members.__init__(self, name, age) # mem is the parent class we reinitilized the parents class init function.

s1 = student("chandana", "25")
s1.newmethod()

#super() this will make the child class inherit all the methods and properties from it's parent\

class student1(members):
    def __init__(initmethod, name, age, field):
        super().__init__(name, age)
        initmethod.field = field
    def new(self):
        print(self.name, self.age, self.field)

s2 = student1("abhi", "25", "DevOps")

s2.new()

# there are 5 types of inheritance
# single
# multiple  - inherting from the multiple parent classes
# multilevel - base, child clas, grand child
# heirarchical
# hybrid

# Iterators
#__iter__() and __next__()

#Iterator vs Iterable
# lists and sets, dict, tuples all aree iterable objects.

string = "akhil"
sudents3 = ("a", "b", "c", "d", "f")
s3 = iter(string)
print(sudents3)
print(next(s3))
print(next(s3))
print(next(s3))
print(next(s3))
print(next(s3))

class mynumbers:
    def __iter__(self):
        self.a = 1
        return self
    def __next__(self):
        if self.a <= 30:
            x = self.a
            self.a += 1
            return x
        else:
            raise StopIteration
mc = mynumbers()
myiter = iter(mc)

for x in myiter:
    print(x)

# modules in python
# 2 types coustom and inbuilt


# exception handling
# try- try to do some execution, except, else, finally
j = 5
try:
    print(j)
except:
    print("some thing else is wrong")
else:
    print("j is accepted")
finally:
    print("try and else, except block is finished")
