import smtplib
import pandas as pd
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import time, random

# Email and password for Gmail account
gmail_address = 'XXX@gmail.com' # Your name
email_subject = "Looking for oppurtunities" # your email subject
gmail_password = '' # your app password
your_name = "sai pradeep" # your name
num_columns_to_process = 5 # no.of mails you want to send at once.  
email_column = 'Work email' # email cloumn name
name_column = 'Names' # name column in the execl sheet
contact = '' # your number

# HTML template
html_template = """
<html>
<head>
  <style>
    body {{
      font-family: Arial, sans-serif;
      line-height: 1.6;
      margin: 0;
      padding: 0;
    }}
    p {{
      margin-bottom: 10px;
    }}
    p {{
        margin-bottom: 20px;
    }}
    .signature {{
        margin-top: 40px;
    }}
  </style>
</head>
<body>
  <p>Hello {name},</p>
  <p>I hope this email finds you well. I am reaching out to express my interest in <b>DevOps</b> opportunities within your organisation. With over 8 years of experience in DevOps and Cloud Engineering, I am eager to contribute my expertise to support the clients' needs.</p>
  <p>Throughout my career, I have demonstrated proficiency in supporting Azure, GCP & AWS environments, implementing best practices in cloud infrastructure, and ensuring application/site reliability for continuous service availability.</p>
  <p>Please feel free to contact me at <b>{contact}</b> or via email at <a href="mailto:{gmail_address}">{gmail_address}</a> if any positions become available. I look forward to discussing potential opportunities with you.</p>
  <p>I am seeking <b>C2C</b> and direct client positions. Thank you for considering my request.</p>
  <p>I am excited about the possibility of working with your company and contributing to its success in fulfilling clients' staffing needs.</p>
  <p>Warm regards,<br>{your_name}</p>
</body>
</html>
"""
def sendmail(to_address, name):
    # Create a connection to the SMTP server
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()

    # Login to your Gmail account
    server.login(gmail_address, gmail_password)

    #Get subject and body
    subject, body = make_msg(name)

    # Create message
    msg = MIMEMultipart()
    msg['From'] = gmail_address
    msg['To'] = to_address
    msg['Subject'] = subject

    # The body of the email
    msg.attach(MIMEText(body, 'html'))

    # Send the email
    server.send_message(msg)

    # Close the connection to the SMTP server
    server.quit()

def make_msg(name):
    subject = email_subject
    body = html_template.format(name=name, your_name=your_name, gmail_address=gmail_address, contact=contact)
    return subject, body

def mails():
    df = pd.read_excel("vendor_list.xlsx")
    emails_to_process = df[email_column][:num_columns_to_process]
    #print(emails_to_process)
    names_to_process = df[name_column]
    for i in range(len(emails_to_process)):
        # Process each column
        email = emails_to_process[i]
        name = names_to_process[i]
        try:
            sendmail(email, name)
            print(f"Processed column: {email, name}")
            time.sleep(random.randrange(5,10))
        except:
            print(f"error processing: {email, name}")

mails()