import platform
import json as j
import sys as s
import datetime as dt
import numpy
import numbers


#print(a.fun_with_param("new", "new1", "this is a module"))


# json module and file operation read and write

# string of json, we can use loads() method to load the json string into a json obj

jsonstring = '{ "name": "akhil", "name1": "abhi", "name3": "manoj"}'

y = j.loads(jsonstring)

print(y["name"])

pythondict = {
    "name": "x",
    "age": 30,
    "field": "unknown"
}
# by using dumps converting python objects to json strings
y1 = j.dumps(pythondict)

y2 = j.loads(y1)

print(y2["name"])

# file handling

f  = open("data.json", "r") # open file in read format

print(y)

x = f.read() # read the data from the json file

print(x)

x1 = j.loads(x) # load string into dict

y_string = j.dumps(y) # conver dict to string

print(type(y_string))

print(type(x1))

x1["name3"] = "mahesh" # update the dict

f1 = open("data.json", "w") # open file in write mode

j.dump(x1, f1, indent=4) # load the data in json file with indentation

#f1.close()

#for i in x:
#    print(i)

# with statement and open function, date and time library
with open("/Users/saipradeepchebrolu/python/new.txt", "a") as txt:
    date = "\nthis event logged at: " + str(dt.datetime.now()) 
    txt.write(date)
    print(txt)

with open("new.txt") as new:
    print(new.read())

